import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './navigation/header/header.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { ProductsComponent } from './dashboard/products/products.component';
import { HowToBuyComponent } from './dashboard/how-to-buy/how-to-buy.component';
import { DumbbellComponent } from './dashboard/dumbbell/dumbbell.component';
import { TreadmillsComponent } from './dashboard/treadmills/treadmills.component';
import { MultiExcersicingComponent } from './dashboard/multi-excersicing/multi-excersicing.component';
import { MultiGymComponent } from './dashboard/multi-gym/multi-gym.component';
import { AbdominalComponent } from './dashboard/abdominal/abdominal.component';
import { YogaComponent } from './dashboard/yoga/yoga.component';
import { FormComponent } from './dashboard/products/form.component';
import { FormsModule } from '@angular/forms';
import { DetailComponent } from './dashboard/products/detail/detail.component';
import { LoginComponent } from './auth/login/login.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { PaymentComponent } from './purchase/payment/payment.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductsComponent,
    HowToBuyComponent,
    DumbbellComponent,
    TreadmillsComponent,
    MultiExcersicingComponent,
    MultiGymComponent,
    AbdominalComponent,
    YogaComponent,
    FormComponent,
    DetailComponent,
    LoginComponent,
    PurchaseComponent,
    PaymentComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
