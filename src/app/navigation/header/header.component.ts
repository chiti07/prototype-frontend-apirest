import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/auth/auth-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  title = 'Your Gym App';

  constructor(public authService: AuthServiceService, private router: Router) { }

  ngOnInit(): void {
  }

  logout(): void{
    this.authService.logout();
    Swal.fire('Logging out', 'Bye bye', 'success');
    this.router.navigate(['/products']);
  }

}
