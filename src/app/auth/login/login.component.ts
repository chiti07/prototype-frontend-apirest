import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthServiceService } from '../auth-service.service';
import { User } from './users';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  title:string = 'Please Sign In';
  user!: User;

  constructor(private authService: AuthServiceService, private router: Router) { 
    this.user = new User();
  }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()){
      Swal.fire('Login', 'You have been already authenticated', 'info');
      this.router.navigate(['/products']);
    }
  }

  login(){
    console.log(this.user);
    if(this.user.username.length == 0 || this.user.password.length == 0){
      Swal.fire('Login error', 'Username or Password fields are empty', 'error');
      return;
    }

    this.authService.login(this.user).subscribe(response =>{
      console.log(response);

      

      this.authService.saveUser(response.access_token);
      this.authService.saveToken(response.access_token);
      let user = this.authService.user;

      this.router.navigate(['/products']);
      Swal.fire('Login', `Hello ${user.username}`, 'success');

      
    }, err => {
      if(err.status == 400){
        Swal.fire('Login Error', 'Wrong credentials', 'error');
      }
    })
    
  }

}
