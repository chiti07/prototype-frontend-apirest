export class User{
    id !: number;
    username !: string;
    password !: string;
    name !: string;
    last_name !: string;
    email !: string; 
    roles:string[] = [];
}