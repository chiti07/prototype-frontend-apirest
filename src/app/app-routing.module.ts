import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AbdominalComponent } from './dashboard/abdominal/abdominal.component';
import { DumbbellComponent } from './dashboard/dumbbell/dumbbell.component';
import { HowToBuyComponent } from './dashboard/how-to-buy/how-to-buy.component';
import { MultiExcersicingComponent } from './dashboard/multi-excersicing/multi-excersicing.component';
import { MultiGymComponent } from './dashboard/multi-gym/multi-gym.component';
import { FormComponent } from './dashboard/products/form.component';
import { ProductsComponent } from './dashboard/products/products.component';
import { TreadmillsComponent } from './dashboard/treadmills/treadmills.component';
import { YogaComponent } from './dashboard/yoga/yoga.component';
import { PaymentComponent } from './purchase/payment/payment.component';
import { PurchaseComponent } from './purchase/purchase.component';

const routes: Routes = [
  { path: 'how-to-buy', component: HowToBuyComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'dumbbell', component: DumbbellComponent},
  { path: 'abdominal', component: AbdominalComponent},
  { path: 'multi-excersicing', component: MultiExcersicingComponent},
  { path: 'multi-gym', component: MultiGymComponent},
  { path: 'treadmills', component: TreadmillsComponent},
  { path: 'yoga', component: YogaComponent},
  { path: 'products/form', component: FormComponent},
  { path: 'products/form/:id', component: FormComponent},
  { path: 'login', component: LoginComponent},
  { path: 'purchase', component: PurchaseComponent},
  { path: 'payment', component: PaymentComponent},






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { } 
