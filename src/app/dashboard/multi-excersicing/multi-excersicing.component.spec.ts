import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiExcersicingComponent } from './multi-excersicing.component';

describe('MultiExcersicingComponent', () => {
  let component: MultiExcersicingComponent;
  let fixture: ComponentFixture<MultiExcersicingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiExcersicingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiExcersicingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
