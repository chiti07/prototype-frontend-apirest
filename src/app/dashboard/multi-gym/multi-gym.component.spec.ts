import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiGymComponent } from './multi-gym.component';

describe('MultiGymComponent', () => {
  let component: MultiGymComponent;
  let fixture: ComponentFixture<MultiGymComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiGymComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiGymComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
