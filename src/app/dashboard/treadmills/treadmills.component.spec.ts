import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreadmillsComponent } from './treadmills.component';

describe('TreadmillsComponent', () => {
  let component: TreadmillsComponent;
  let fixture: ComponentFixture<TreadmillsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreadmillsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreadmillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
