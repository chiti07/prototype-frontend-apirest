import { Injectable } from '@angular/core';
import { Product } from './product';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators'
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/auth/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private urlEndpoint: string = 'http://localhost:8080/stock-api/products';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  constructor(private http: HttpClient, private router: Router, private authService: AuthServiceService) { }

  private addAuthorizationHeader() {
    let token = this.authService.token;
    if(token != null){
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAuth(e: { status: number; }): boolean{
    if(e.status == 401){

      if(this.authService.isAuthenticated()){
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }
    if(e.status == 403){
      swal.fire('Access Denied', 'You do not have access to this resource', 'warning');
      this.router.navigate(['']);
      return true;
    }

    return false;
  }

  getProducts(): Observable<Product[]>{
      return this.http.get<Product[]>(this.urlEndpoint, {headers: this.addAuthorizationHeader()}).pipe(
        catchError(e => {
          this.isNoAuth(e);
          return throwError(e);
        })
      )
  }

  create(product: Product) : Observable<Product>{
    return this.http.post<Product>(this.urlEndpoint, product, {headers: this.addAuthorizationHeader()})
    .pipe(
      map((response: any) => response.product as Product),
      catchError(e=> {
        if(this.isNoAuth(e)){
          return throwError(e);
        }

        if(e.status == 400){
          return throwError(e);
        }
        console.error(e.console.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);

        
      })
    )
  }

  getProduct(id: number): Observable<Product>{
    return this.http.get<Product>(`${this.urlEndpoint}/${id}`, {headers: this.addAuthorizationHeader()})
    .pipe(
      catchError(e=> {
        if(this.isNoAuth(e)){
          return throwError(e);
        }

        if(e.status == 400){
          return throwError(e);
        }
        console.error(e.console.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);

        
      })
    )
  }

  update(product: Product): Observable<Product>{
    return this.http.put<Product>(`${this.urlEndpoint}/${product.id}`, product, {headers: this.addAuthorizationHeader()})
     .pipe(
       catchError(e=> {
         if(this.isNoAuth(e)){
           return throwError(e);
         }

         if(e.status == 400){
           return throwError(e);
         }
         console.error(e.console.mensaje);
         swal.fire(e.error.mensaje, e.error.error, 'error');
         return throwError(e);

        
       })
     )
  }

  delete(id: number): Observable<Product>{
    return this.http.delete<Product>(`${this.urlEndpoint}/${id}`, {headers: this.addAuthorizationHeader()})
     .pipe(
       catchError(e=> {
         if(this.isNoAuth(e)){
           return throwError(e);
         }

         if(e.status == 400){
           return throwError(e);
         }
         console.error(e.console.mensaje);
         swal.fire(e.error.mensaje, e.error.error, 'error');
         return throwError(e);

        
       })
     )
  }

  uploadPicture(file: File, id: number): Observable<HttpEvent<{}>>{
    let formData = new FormData();
    formData.append("file", file);
    formData.append("id", id.toString());

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;

    if(token!= null){
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }

    const req = new HttpRequest('POST', `${this.urlEndpoint}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });

    
    return this.http.request(req);

  }

}
