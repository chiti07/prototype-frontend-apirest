import { HttpEventType, HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
 import { Product } from '../product';
import { ProductService } from '../product.service';
import swal from 'sweetalert2';
import { ModalService } from './modal.service';
import { AuthServiceService } from 'src/app/auth/auth-service.service';


@Component({
  selector: 'product-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @Input() product!: Product;
  title: string = "Product Detail";
  public selectedPicture !: File;
  progress: number = 0;

  constructor(
    private productService: ProductService,
    public modalService: ModalService,
    public authService: AuthServiceService) { }

  ngOnInit(): void {
    
  }

  selectPicture(event: any) {
    this.selectedPicture = event.target.files[0];
    this.progress = 0;
    console.log(this.selectedPicture);
    if(this.selectedPicture.type.indexOf('image') < 0){
      swal.fire('Error: ', `You should select a image type file`, 'error');
      this.selectedPicture = null!;
    }
  }

  uploadPicture() {
    if (!this.selectedPicture) {
      swal.fire('Error: ', `You should select the picture`, 'error')

    } else {
      this.productService.uploadPicture(this.selectedPicture, this.product.id)
        .subscribe(event => {
          if(event.type === HttpEventType.UploadProgress){
            this.progress = Math.round((event.loaded/event.total!)*100)
          }else if(event.type === HttpEventType.Response){
            let response: any = event.body;
            this.product = response.product as Product;

            this.modalService.notificationUpload.emit(this.product);
            swal.fire('Picture updated', response.mensaje, 'success')

          }

        })
    }
  }

  closeModal(){
    this.modalService.closeModal();
    this.selectedPicture = null!;
    this.progress = 0;
  }
}




