export class Product {
    id!: number;
    name!: string;
    description!: string;
    amount!: number;
    price: number = 0.0;
    lastUpdated!: string;
    category!: string;
    picture !: string;

}
