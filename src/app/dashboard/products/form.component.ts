import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from './product';
import { ProductService } from './product.service';
import swal from 'sweetalert2';

interface Categories {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public product: Product = new Product()
  public title: string = "Create a New Product"

  constructor(private productService: ProductService,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getProduct();
  }

  categories: Categories[] = [
    {value: 'dumbbels', viewValue: 'Dumbbells'},
    {value: 'treadmills', viewValue: 'Treadmills'},
    {value: 'multi-gym', viewValue: 'Multi-Gym Machines'},
    {value: 'abdominal', viewValue: 'Abdominal Equipment'},
    {value: 'yoga', viewValue: 'Yoga'}

  ];

  public create(): void {
    this.productService.create(this.product).subscribe(
      response => {
        this.router.navigate(['/products'])
        swal.fire('Product Saved', `Product ${this.product.name} created successfully`, 'success')
      }
    )
  }

  getProduct(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.productService.getProduct(id).subscribe((product) => this.product = product)
      }

    })

  }

  update(): void{
    console.log(this.product);
    
    this.productService.update(this.product).subscribe(
      product => {
        this.router.navigate(['/products'])
        //swal.fire('Product Updated', `${product.name} updated successfully`, 'info')
      }
    )
  }

  
}
