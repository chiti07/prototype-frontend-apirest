import { Component, OnInit } from '@angular/core';
import { Product } from './product';
import { ProductService } from './product.service';
import swal from 'sweetalert2'
import { ModalService } from './detail/modal.service';
import { AuthServiceService } from 'src/app/auth/auth-service.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];

  selectedProduct !: Product;

  constructor(private productService: ProductService, private modalService: ModalService, public authService: AuthServiceService ) { }

  ngOnInit(): void {
     this.productService.getProducts().subscribe(
       (products) => {
         this.products = products
       }
      );

      this.modalService.notificationUpload.subscribe((product: { id: number; picture: string; }) => {
        this.products.map(originalProduct => {
          if(product.id == originalProduct.id){
            originalProduct.picture = product.picture;
          }
          return originalProduct;
        })
      })
  }

  delete(product: Product): void{
    const swalWithBootstrapButtons = swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Do you want to delete this product?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.productService.delete(product.id).subscribe(
          response => {
            this.products = this.products.filter(cli => cli !== product)
            swalWithBootstrapButtons.fire(
            'Product Deleted!',
            `The product: ${product.name} was deleted`,
            'success'
          )}
        )
        
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'The product was not deleted',
          'error'
        )
      }
    })


  }

  openModal(product: Product){
    this.selectedProduct = product;
    this.modalService.openModal();
  }
}
